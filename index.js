const Express = require("express");
const app = Express();

const knex = require("knex")({
  client: "mysql",
  connection: {
    host: "localhost",
    port: 3325,
    user: "root",
    password: "",
    database: "employee_simple_database",
  },
});

app.use(Express.json());

// checking the connection of database
knex
  .raw("SELECT * from employee_table")
  .then(() => console.log("Connection successfull"));
//display data in database
app.get("/database", async (req, res) => {
  try {
    const Emp_Details = await knex("employee_table");
    res.json(Emp_Details);
  } catch (e) {
    console.log(e);
  }
});
//insert data in database
app.post("/insert", async (req, res) => {
  try {
    const empName = req.body.EmpName;
    const DOB = req.body.DOB;
    const empMobileNo = req.body.Phno;
    const empEmailID = req.body.MailID;

    await knex("employee_table").insert({
      Employee_Name: empName,
      DOB: DOB,
      PhoneNo: empMobileNo,
      Email_ID: empEmailID,
    });
    res.send("Employee is Added Successfully.");
  } catch (e) {
    console.log(e);
  }
});
// delete data in database
app.delete("/database/:id", async (req, res) => {
  const Id = req.params.id;
  const Emp = await knex("employee_table").where({ Employee_ID: Id });
  await knex("employee_table").where({ Employee_ID: Id }).del();
  res.send(Emp);
});
// update data in database
app.put("/database/:id", async (req, res) => {
  const Id = req.params.id;
  const empName = req.body.EmpName;
  const DOB = req.body.DOB;
  const empMobileNo = req.body.Phno;
  const empEmailID = req.body.MailID;

  await knex("employee_table").where({ Employee_ID: Id }).update({
    Employee_Name: empName,
    DOB: DOB,
    PhoneNo: empMobileNo,
    Email_ID: empEmailID,
  });
  const Emp = await knex("employee_table").where({ Employee_ID: Id });

  res.send(Emp);
});

app.get("", (req, res) => res.send("Workings Good."));

const port = 5000;
app.listen(port, () => console.log(`Listening to port: ${port} `));
